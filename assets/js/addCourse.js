let addCourseForm = document.querySelector("#createCourse")

addCourseForm.addEventListener("submit", (e) => {
    e.preventDefault()
    
    let courseName = document.querySelector("#courseName").value
    let coursePrice = document.querySelector("#coursePrice").value 
    let courseDesc = document.querySelector("#courseDesc").value
    

    //lets create a validation to enable the submit button when all fields are populated and both passwords should match otherwise it should not continue.
    if((courseName !== '' && coursePrice !== '' && courseDesc !== '')) {

        //if all the requirements are met, then now lets check for duplicate emails in the database first. 
        fetch('http://localhost:4000/api/courses/course-exists', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
                courseName: courseName,
            })
        })
        .then(res => res.json())
        .then(data => {
            //if no email duplicates are found 
            if(data === false){
                console.log(data)
                fetch('http://localhost:4000/api/courses', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        courseName: courseName,
                        coursePrice: coursePrice,
                        courseDesc : courseDesc
                        
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    console.log(data)
                    //lets give a proper response if registration will become successful
                    if(data === true){
                        alert("New Course has been added successfully")
                        //redirect to login 
                        window.location.replace("./courses.html")
                    } else {
                        alert("Something Went Wrong in the Registration!")
                    }
                })
            }
        })
    } else {
      alert("Something went wrong, check credentials!")  
    }
})