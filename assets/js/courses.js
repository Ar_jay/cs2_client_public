let token = localStorage.getItem("token")

let courseContainer = document.querySelector("#courseContainer");


if(!token || token === null ) {
        alert("Please login first");
        window.location.href = "./login.html"
} else {
        fetch('http://localhost:4000/api/courses', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                 'Authorization': `Bearer ${token}`
            }
            }).then(res => res.json())
              .then(data => {
                let courseData = data.map(classData => {
                        return(`<tr>
                                   <h3 class="text-center">Course Name: ${classData.courseName}</h3>
                                    <h3 class="text-center">Course Description: ${classData.courseDesc}</h3>
                                    <h3 class="text-center">Price: ${classData.coursePrice}</h3>
                                    <button class= "text-center"> Enroll </button> 
                                </tr>`)
                })  
                courseContainer.innerHTML = `<div id="divInner" class="com-md-12">
                                <section class="jumbotron my-5">
                                        <table class="table">
                                                <thead>
                                                        <tr>
                                                                            
                                                        </tr>
                                                        <tbody>
                                                           ${courseData}
                                                        </tbody>
                                                </thead>
                                        </table>
                                </section>
                        </div>`
                })
        }