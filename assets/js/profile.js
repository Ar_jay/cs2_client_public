let token = localStorage.getItem("token")

let profileContainer = document.querySelector("#profileContainer");

if(!token || token === null ) {
        alert("Please login first");
        window.location.href = "./login.html"
} else {
        fetch('http://localhost:4000/api/users/details', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                 'Authorization': `Bearer ${token}`
            }
            }).then(res => res.json())
              .then(data => {
                let enrollmentData = data.enrollments.map(classData => {
                        return(`<tr>
                                   <td>${classData.courseId}</td>
                                   <td>${classData.enrolledOn}</td>
                                   <td>${classData.status}</td>
                                </tr>`)
                }).join('')     
                profileContainer.innerHTML = `<div id= "divInner" class="col-md-12">
                                <section class="jumbotron my-2">
                                        <img id="userLogo" src="../images/userLogo.png" alt="k u r s o">
                                        <h3 class="text-right"><b>First Name:</b> ${data.firstName}</h3>
                                        <h3 class="text-right"><b>Last Name:</b> ${data.lastName}</h3>
                                        <h3 class="text-right"><b>Email:</b> ${data.email}</h3><br>
                                        <h3 class="text-left">Class History</h3>
                                        <table class="table">
                                                <thead>
                                                        <tr>
                                                                <th> CourseID </th>
                                                                <th> Enrolled On </th>
                                                                <th> Status </th>
                                                        </tr>
                                                        <tbody>
                                                                ${enrollmentData}
                                                        </tbody>
                                                </thead>
                                        </table>
                                </section>
                        </div>`
                })
        }